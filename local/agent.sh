#!/bin/bash
#
# File: agent.sh -> builds and deploys SpringBoot apps on local Jenkins server
#
# Author: Frederick Ofosu-Darko <frederick.ofosu-darko@minex360.com, fofosudarko@gmail.com>
#
# Usage: bash agent.sh <LOCAL_PROJECT> <JAVA_EXECUTABLE_PATH> <APP_NAME> <APP_DESCRIPTION> <APP_DB_CONNECTION_POOL>
#
#

## - start here

# jenkins pulls springboot repository to a workspace
# copy workspace to a defacto springboot apps directory
# change to that directory
# build springboot project
# run app service

getAppService ()
{
  sudo su $SPRINGBOOT_USER -c cat <<EOF
[Unit]
Description=${APP_DESCRIPTION}

[Service]
WorkingDirectory=${APP_DIR}
ExecStart=/bin/sh -c '${JAVA_EXECUTABLE_PATH} -jar ${APP_DIR}/${APP_NAME}.jar'
Restart=on-failure
RestartSec=10
User=${SPRINGBOOT_USER}
SyslogIdentifier=${LOCAL_PROJECT}
Environment=
StandardOutput=file:${LOGFILE}
StandardError=file:${LOGFILE}

[Install]
WantedBy=multi-user.target
EOF
}

writeAppService ()
{
  cat $STDIN | sudo -u $SPRINGBOOT_USER tee $APP_SERVICE
}

COMMAND=$0

if [[ "$#" != 5 ]]
then
  echo >&2 "$COMMAND: expects 5 arguments ie. LOCAL_PROJECT JAVA_EXECUTABLE_PATH APP_NAME APP_DESCRIPTION APP_DB_CONNECTION_POOL"
  exit 2
fi

LOCAL_PROJECT=$1
JAVA_EXECUTABLE_PATH=$2
APP_NAME=$3
APP_DESCRIPTION=$4
APP_DB_CONNECTION_POOL=${5:-'transaction'}

: ${JENKINS_WORKSPACE_DIR='/var/lib/jenkins/workspace'}
: ${PROJECT_DIR="${JENKINS_WORKSPACE_DIR}/${LOCAL_PROJECT}"}
: ${DEPLOY_ROOTDIR='/var/lib/springboot'}
: ${APPS_DIR="${DEPLOY_ROOTDIR}/apps"}
: ${APP_DIR="${APPS_DIR}/${LOCAL_PROJECT}"}
: ${SPRINGBOOT_USER='springboot'}
: ${SERVICE_UNIT="${LOCAL_PROJECT}.service"}
: ${APP_SERVICE="${APP_DIR}/${SERVICE_UNIT}"}
: ${SYSTEMD_SERVICE="/etc/systemd/system/${SERVICE_UNIT}"}
: ${LOGDIR="${DEPLOY_ROOTDIR}/logs"}
: ${LOGFILE="${LOGDIR}/${LOCAL_PROJECT}.log"}
: ${TARGET_BUILD="${APP_DIR}/target/${APP_NAME}*.jar"}
: ${DEPLOY_BUILD="${APP_DIR}/${APP_NAME}.jar"}
: ${SOURCE_PROPERTIES="${APP_DIR}/application.properties"}
: ${BUILD_PROPERTIES="${APP_DIR}/src/main/resources/application.properties"}
: ${DEPLOY_PROJECT_SRCDIR="${APP_DIR}/src"}

echo $COMMAND: Checking existence of project directory...

if [ ! -d "$PROJECT_DIR" ]
then
  echo >&2 "$COMMAND: No project found in Jenkins workspace. Build aborted."
  exit 1
fi

echo $COMMAND: Creating application\'s directory if there is none...

if [ ! -d "$APP_DIR" ]
then
  sudo su "$SPRINGBOOT_USER" -c "mkdir -p $APP_DIR"
fi

echo $COMMAND: Cleaning application\'s src directory...

if [ -d "$DEPLOY_PROJECT_SRCDIR" ]
then
  sudo su "$SPRINGBOOT_USER" -c "rm -rf $DEPLOY_PROJECT_SRCDIR/*"
fi

echo $COMMAND: Copying changesets to application\'s deploy location...

sudo su "$SPRINGBOOT_USER" -c "cp -ap ${PROJECT_DIR}/* ${APP_DIR}"

echo $COMMAND: Changing to application\'s deploy location...

cd "${APP_DIR}"

echo $COMMAND: Stopping application based on app db connection pool...

if [[ "$APP_DB_CONNECTION_POOL" == "session" ]]
then
  sudo systemctl stop $SERVICE_UNIT
  echo $COMMAND: Application service $SERVICE_UNIT stopped
  sleep 2
fi

echo $COMMAND: Refreshing and installing application\'s dependencies...

if [ ! -f "$SOURCE_PROPERTIES" ]
then
  echo >&2 "$COMMAND: No application properties found. Build aborted."
  exit 2
fi

sudo su $SPRINGBOOT_USER -c "cp -ap $SOURCE_PROPERTIES $BUILD_PROPERTIES"

sudo su $SPRINGBOOT_USER -c 'mvn clean install package'

echo $COMMAND: Checking build...

buildStatus=$(sudo su $SPRINGBOOT_USER -c "dir -1 $TARGET_BUILD > /dev/null 2>&1 && echo -ne \$?")

if [[ $buildStatus != 0 ]]
then
  echo >&2 "$COMMAND: Build failed"
  exit 2
fi

sudo su $SPRINGBOOT_USER -c "cp -ap $TARGET_BUILD $DEPLOY_BUILD"

if [[ ! -e "$SYSTEMD_SERVICE" ]]
then
  echo >&2 $COMMAND: Writing systemd service unit for $APP_NAME...
  
  getAppService | writeAppService
  
  sudo cp -ap $APP_SERVICE $SYSTEMD_SERVICE
  sudo systemctl reset-failed
  sudo systemctl daemon-reload
  sudo systemctl enable $SERVICE_UNIT
  sudo systemctl start $SERVICE_UNIT
else
  sudo systemctl restart $SERVICE_UNIT
fi

sudo systemctl status $SERVICE_UNIT

exit 0

## -- finish
