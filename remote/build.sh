#!/bin/bash
#
# File: build.sh -> builds and deploys SpringBoot apps on production server
#
# Author: Frederick Ofosu-Darko <frederick.ofosu-darko@minex360.com, fofosudarko@gmail.com>
#
# Usage: bash agent.sh <JENKINS_JOB> <JAVA_EXECUTABLE_PATH> <APP_NAME> <APP_DESCRIPTION>
#
#

## - start here

# jenkins pulls springboot repository to a workspace
# copy workspace to a defacto springboot apps directory
# change to that directory
# build springboot project
# run app service

getAppService ()
{
  local deployBuild="$1"
  local appPort="$2"
  local appDescription="$3"
  local syslogIdentifier="$4"
  
  sudo su $SPRINGBOOT_USER -c cat <<EOF
[Unit]
Description=${appDescription}

[Service]
WorkingDirectory=${APP_DIR}
ExecStart=${JAVA_EXECUTABLE_PATH} -jar ${deployBuild} --server.port=${appPort}
Restart=on-failure
RestartSec=10
User=${SPRINGBOOT_USER}
SyslogIdentifier=${syslogIdentifier}
Environment=
StandardOutput=file:${LOGFILE}
StandardError=file:${LOGFILE}

[Install]
WantedBy=multi-user.target
EOF
}

writeAppService ()
{
  cat $STDIN | sudo -u $SPRINGBOOT_USER tee $APP_SERVICE
}

deployAppService ()
{
  local systemdService="$1"
  local appName="$2"
  local appPort="$3"
  local appDescription="$4"
  local appService="$5"
  local jenkinsJob="$6"
  local deployBuild="$7"
  local serviceUnit="$8"
  
  if [[ ! -e "$systemdService" ]]
  then
    echo >&2 $COMMAND: Writing systemd service unit for $appName...
  
    getAppService "$deployBuild" "$appPort" "$appDescription" "$jenkinsJob" | \
    writeAppService

    sudo cp -ap $appService $systemdService
  
    sudo systemctl reset-failed
    sudo systemctl daemon-reload
    sudo systemctl enable $serviceUnit
    sudo systemctl start $serviceUnit
  else
    sudo systemctl restart $serviceUnit
  fi

  sudo systemctl status $serviceUnit

  # show application is running
  sudo netstat -tulpn|grep -P ":${appPort}"
}

COMMAND=$0

if [[ "$#" != 5 ]]
then
  echo >&2 "$COMMAND: expects 5 arguments ie. JENKINS_JOB JAVA_EXECUTABLE_PATH APP_NAME APP_PORT APP_DESCRIPTION"
  exit 2
fi

JENKINS_JOB=$1
JAVA_EXECUTABLE_PATH=$2
APP_NAME=$3
APP_PORT=$4
APP_DESCRIPTION=$5

: ${JENKINS_HOME='/home/jenkins'}
: ${JENKINS_WORKSPACE_DIR="${JENKINS_HOME}/workspace"}
: ${JENKINS_BACKUP_DIR="${JENKINS_HOME}/backup"}
: ${JENKINS_BACKUP_APPS_DIR="${JENKINS_BACKUP_DIR}/apps"}
: ${PROJECT_DIR="${JENKINS_WORKSPACE_DIR}/${JENKINS_JOB}"}
: ${SPRINGBOOT_HOME='/var/lib/springboot'}
: ${APPS_DIR="${SPRINGBOOT_HOME}/apps"}
: ${APP_DIR="${APPS_DIR}/${JENKINS_JOB}"}
: ${SPRINGBOOT_USER='springboot'}
: ${JENKINS_USER='jenkins'}
: ${SERVICE_UNIT="${JENKINS_JOB}.service"}
: ${APP_SERVICE="${APP_DIR}/${SERVICE_UNIT}"}
: ${SYSTEMD_SERVICE="/etc/systemd/system/${SERVICE_UNIT}"}
: ${LOGDIR="${SPRINGBOOT_HOME}/logs"}
: ${LOGFILE="${LOGDIR}/${JENKINS_JOB}.log"}
: ${TARGET_BUILD="${APP_DIR}/target/${APP_NAME}*.jar"}
: ${DEPLOY_BUILD="${APP_DIR}/${APP_NAME}.jar"}
: ${DEPLOY_BUILD_BACKUP="${JENKINS_BACKUP_APPS_DIR}/${APP_NAME}.jar"}
: ${SOURCE_PROPERTIES="${APP_DIR}/application.properties"}
: ${BUILD_PROPERTIES="${APP_DIR}/src/main/resources/application.properties"}
: ${BUILD_MORE_INSTANCES="${APP_DIR}/more_build_instances"}
: ${DEPLOY_PROJECT_SRCDIR="${APP_DIR}/src"}
: ${BUILD_DEPLOY_PAUSE=2}

echo $COMMAND: Checking existence of project directory...

if [ ! -d "$PROJECT_DIR" ]
then
  echo >&2 "$COMMAND: No project found in Jenkins workspace. Build aborted."
  exit 1
fi

echo $COMMAND: Creating application\'s directory if there is none...

if [ ! -d "$APP_DIR" ]
then
  sudo su "$SPRINGBOOT_USER" -c "mkdir -p $APP_DIR"
fi

echo $COMMAND: Cleaning application\'s src directory...

if [ -d "$DEPLOY_PROJECT_SRCDIR" ]
then
  sudo su "$SPRINGBOOT_USER" -c "rm -rf $DEPLOY_PROJECT_SRCDIR/*"
fi

echo $COMMAND: Copying changesets to application\'s deploy location...

sudo su "$SPRINGBOOT_USER" -c "cp -ap ${PROJECT_DIR}/* ${APP_DIR}"

echo $COMMAND: Changing to application\'s deploy location...

cd "${APP_DIR}"

echo $COMMAND: Refreshing and installing application\'s dependencies...

if [ ! -f "$SOURCE_PROPERTIES" ]
then
  echo >&2 "$COMMAND: No application properties found. Build aborted."
  exit 2
fi

sudo su $SPRINGBOOT_USER -c "cp -ap $SOURCE_PROPERTIES $BUILD_PROPERTIES"

sudo su $SPRINGBOOT_USER -c 'mvn clean install package'

echo $COMMAND: Checking build...

buildStatus=$(sudo su $SPRINGBOOT_USER -c "dir -1 $TARGET_BUILD > /dev/null 2>&1 && echo -ne \$?")

# revert to using latest working version before build process
if [[ "${buildStatus}" != 0 ]]
then
  echo >&2 "$COMMAND: Build and deploy failed."

  if [ ! -e "${DEPLOY_BUILD_BACKUP}" ]
  then
    echo >&2 'No backup build found.'
    exit 0
  fi

  echo >&2 "$COMMAND: Reverting to latest working version..."
  
  sudo -u $SPRINGBOOT_USER cp -ap "${DEPLOY_BUILD_BACKUP}" "${DEPLOY_BUILD}" 
else
  sudo su $SPRINGBOOT_USER -c "cp -ap $TARGET_BUILD $DEPLOY_BUILD"
fi

# pause build
sleep "${BUILD_DEPLOY_PAUSE}"

deployAppService \
  "$SYSTEMD_SERVICE" "$APP_NAME" "$APP_PORT" "$APP_DESCRIPTION" \
  "$APP_SERVICE" "$JENKINS_JOB" "$DEPLOY_BUILD" "$SERVICE_UNIT"

echo >&2 "$COMMAND: Build and deploy done."

exit 0

## -- finish
