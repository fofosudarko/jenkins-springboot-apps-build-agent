#!/bin/bash -e
#
# File: agent.sh -> transfers source files from staging server to production server
#
# Author: Frederick Ofosu-Darko <frederick.ofosu-darko@minex360.com, fofosudarko@gmail.com>
#
# Usage: bash agent.sh <BUILD_COMMAND> <JAVA_EXECUTABLE_PATH> <JENKINS_JOB_NAME> <IP_OF_REMOTE_SERVER> <APP_NAME> <APP_PORT> <APP_DESCRIPTION> <REMOTE_JENKINS_PROJECT_DATABASE> 
#
#

## - start here


runRemoteCommand ()
{
  $RUN_REMOTE_COMMAND "$1"
}

remoteFileExists ()
{
  $RUN_REMOTE_COMMAND <<EOF
if [[ -e "${1}" ]]
then
  echo 0
else
  echo 1
fi
EOF
}

backupRemoteProject ()
{
  runRemoteCommand "zip -r "${remoteProjectBackupDestination}" "${remoteProjectBackupSource}" 2>&1"
}

removeRemoteProject ()
{
  runRemoteCommand "rm -rf "${REMOTE_JENKINS_PROJECT}" 2>&1"
}

backupRemoteProjectDatabase ()
{
  $RUN_REMOTE_COMMAND <<EOF
  pg_dump -w -U postgres --format=c --data-only --file="${REMOTE_JENKINS_PROJECT_DATABASE_BACKUP_DATA}" \
    "${REMOTE_JENKINS_PROJECT_DATABASE}" # dump project database data
  pg_dump -w -U postgres --format=c --schema-only --create --file="${REMOTE_JENKINS_PROJECT_DATABASE_BACKUP_SCHEMA}" \
    "${REMOTE_JENKINS_PROJECT_DATABASE}" # dump project database schema
EOF
}

createRemoteDirectories ()
{
  $RUN_REMOTE_COMMAND <<EOF
  for directory in \
    "${REMOTE_JENKINS_WORKSPACE_DIR}" \
    "${REMOTE_JENKINS_BACKUP_DIR}" \
    "${REMOTE_JENKINS_BACKUP_SOURCE_DIR}" \
    "${REMOTE_JENKINS_BACKUP_DB_DIR}" \
    "${REMOTE_JENKINS_BACKUP_APPS_DIR}" \
    "${REMOTE_JENKINS_PROJECT}" \
    "${REMOTE_JENKINS_PROJECT_DATABASE_BACKUP}" \
    "${REMOTE_JENKINS_PROJECT_APP_BACKUP}"
  do
    if [ ! -d \$directory ]
    then
      mkdir -vp \$directory
    fi
  done
EOF
}

backupAndBuildAndDeployRemoteProject ()
{
  $RUN_REMOTE_COMMAND <<EOF
  # backup running build
  sudo cp -ap "${REMOTE_JENKINS_PROJECT_BUILD}" "${REMOTE_JENKINS_PROJECT_APP_BACKUP}/$(basename ${REMOTE_JENKINS_PROJECT_BUILD})"
  
  # test app backup is successful
  if [[ ! -e "${REMOTE_JENKINS_PROJECT_APP_BACKUP}/$(basename ${REMOTE_JENKINS_PROJECT_BUILD})" ]]
  then
    echo >&2 "No '${REMOTE_JENKINS_PROJECT_BUILD}' to backup."
  fi
  
  # copy project to build dir
  sudo -u "$SPRINGBOOT_USER" cp -ap "${REMOTE_JENKINS_PROJECT}"/* "${REMOTE_JENKINS_PROJECT_BUILD_DIR}"

  # run build script
  sudo /bin/bash ${BUILD_SCRIPT} '$JENKINS_JOB_NAME' '$JAVA_EXECUTABLE_PATH' '$APP_NAME' '$APP_PORT' '$APP_DESCRIPTION'
EOF
}

COMMAND=$0
BUILD_COMMAND=$1
JAVA_EXECUTABLE_PATH=$2
JENKINS_JOB_NAME=$3
REMOTE_SERVER_IP=$4
APP_NAME=$5
APP_PORT=$6
APP_DESCRIPTION=$7
REMOTE_JENKINS_PROJECT_DATABASE=$8
RUNTIME_AT=`date +'%Y%m%d%H%M%S'`
BUILD_RUNTIME=${RUNTIME_AT}
DEPLOY_RUNTIME=${RUNTIME_AT}

# check number of arguments passed
if [[ "$#" != 8 ]]
then
  echo -ne "$COMMAND: expects 8 arguments i.e. <BUILD_COMMAND> <JAVA_EXECUTABLE_PATH> <JENKINS_JOB_NAME> "
  echo -ne "<IP_OF_REMOTE_SERVER> <APP_NAME> <APP_PORT> <APP_DESCRIPTION> <REMOTE_JENKINS_PROJECT_DATABASE>\n" >&2
  exit 1
fi

: ${REMOTE_SERVER_SSH_PORT=3600}
: ${JENKINS_WORKSPACE='/var/lib/jenkins/workspace'}
: ${JENKINS_USER='jenkins'}
: ${SPRINGBOOT_USER='springboot'}
: ${SPRINGBOOT_HOME='/var/lib/springboot'}
: ${RSYNC_SSH="ssh -p ${REMOTE_SERVER_SSH_PORT}"}
: ${REMOTE_JENKINS_HOME='/home/jenkins'}
: ${REMOTE_JENKINS_WORKSPACE_DIR="${REMOTE_JENKINS_HOME}/workspace"}
: ${REMOTE_JENKINS_BACKUP_DIR="${REMOTE_JENKINS_HOME}/backup"}
: ${REMOTE_JENKINS_BACKUP_SOURCE_DIR="${REMOTE_JENKINS_BACKUP_DIR}/src"}
: ${REMOTE_JENKINS_BACKUP_DB_DIR="${REMOTE_JENKINS_BACKUP_DIR}/postgres"}
: ${REMOTE_JENKINS_BACKUP_APPS_DIR="${REMOTE_JENKINS_BACKUP_DIR}/apps"}
: ${REMOTE_JENKINS_PROJECT_BUILD_DIR="$SPRINGBOOT_HOME/apps/${JENKINS_JOB_NAME}"}
: ${REMOTE_JENKINS_PROJECT_BUILD="${REMOTE_JENKINS_PROJECT_BUILD_DIR}/${APP_NAME}.jar"}
: ${RUN_REMOTE_COMMAND="ssh -T -p ${REMOTE_SERVER_SSH_PORT} ${JENKINS_USER}@${REMOTE_SERVER_IP}"}
: ${BUILD_SCRIPT="${REMOTE_JENKINS_HOME}/scripts/jenkins-springboot-apps-build-agent/remote/build.sh"}
: ${BUILD_DEPLOY_PAUSE=2}
: ${SHOW_RUNTIME_LOGS=3}
: ${SHOW_RUNTIME_LOGS_NUMBER=100}

JENKINS_JOB="${JENKINS_WORKSPACE}/${JENKINS_JOB_NAME}"
REMOTE_JENKINS_PROJECT="${REMOTE_JENKINS_WORKSPACE_DIR}/${JENKINS_JOB_NAME}"
REMOTE_JENKINS_PROJECT_DATABASE_BACKUP="${REMOTE_JENKINS_BACKUP_DB_DIR}/${REMOTE_JENKINS_PROJECT_DATABASE}"
REMOTE_JENKINS_PROJECT_APP_BACKUP="${REMOTE_JENKINS_BACKUP_APPS_DIR}/${JENKINS_JOB_NAME}"
REMOTE_JENKINS_PROJECT_DATABASE_BACKUP_SCHEMA="${REMOTE_JENKINS_PROJECT_DATABASE_BACKUP}/springboot-apps-${REMOTE_JENKINS_PROJECT_DATABASE}.${BUILD_RUNTIME}.schema"
REMOTE_JENKINS_PROJECT_DATABASE_BACKUP_DATA="${REMOTE_JENKINS_PROJECT_DATABASE_BACKUP}/springboot-apps-${REMOTE_JENKINS_PROJECT_DATABASE}.${BUILD_RUNTIME}.data"
JENKINS_JOB_BUILD_CONFIG="${JENKINS_JOB}/pom.xml"
APP=
REMOTE_BACKUP_APP=

# check build command
if ! echo -n "$BUILD_COMMAND" | grep -qP '^(deploy)$'
then
  echo >&2 "$COMMAND: Build command '$BUILD_COMMAND' invalid. Should be 'deploy'"
  exit 1
fi

# check project's existence
if [[ ! -d "$JENKINS_JOB" ]]
then
  echo >&2 "$COMMAND: $JENKINS_JOB not found."
  exit 1
fi

# check remote server ip
if ! echo -n "$REMOTE_SERVER_IP" | grep -qP '^\d{3}\.\d{3}\.\d{3}\.\d{3}$'
then
  echo >&2 "$COMMAND: Remote IP '$REMOTE_SERVER_IP' invalid."
  exit 1
fi

# create remote directories if there are none
createRemoteDirectories

# check whether project exists on remote server
projectExists=$(remoteFileExists "$REMOTE_JENKINS_PROJECT")

if [[ $projectExists == 0 ]]
then
  # backup old project
  echo "Backup remote project '${REMOTE_JENKINS_PROJECT}'..."
  
  remoteProjectBackupDestination="${REMOTE_JENKINS_BACKUP_DIR}/${JENKINS_JOB_NAME}.${BUILD_RUNTIME}.zip"
  remoteProjectBackupSource="${REMOTE_JENKINS_PROJECT}"
  backupRemoteProject
  backupSucceeded=$(remoteFileExists "$remoteProjectBackupDestination")
  
  if [[ $backupSucceeded != 0 ]]
  then
    echo >&2 "$COMMAND: Remote backup of $JENKINS_JOB_NAME failed."
    exit 1
  fi

  # remove old project from REMOTE_JENKINS_WORKSPACE_DIR
  echo "Remove remote project '${REMOTE_JENKINS_PROJECT}'..."
  
  removeRemoteProject
  removeSucceeded=$(remoteFileExists "$REMOTE_JENKINS_PROJECT")
  
  if [[ $removeSucceeded != 1 ]]
  then
    echo >&2 "$COMMAND: Could not remove $JENKINS_JOB_NAME after backup."
    exit 1
  fi
fi

# copy project with latest changesets to remote dir
echo "Copy ${JENKINS_JOB} to ${REMOTE_JENKINS_WORKSPACE_DIR}..."

rsync -av --progress -e "$RSYNC_SSH" --exclude='.git/' --exclude='target/' \
  ${JENKINS_JOB} ${JENKINS_USER}@${REMOTE_SERVER_IP}:${REMOTE_JENKINS_WORKSPACE_DIR}

# backup database
echo "Backup project database '$REMOTE_JENKINS_PROJECT_DATABASE' to ${REMOTE_JENKINS_PROJECT_DATABASE_BACKUP}..."

backupRemoteProjectDatabase
backupSchemaSucceeded=$(remoteFileExists "$REMOTE_JENKINS_PROJECT_DATABASE_BACKUP_SCHEMA")
backupDataSucceeded=$(remoteFileExists "$REMOTE_JENKINS_PROJECT_DATABASE_BACKUP_DATA")

if [[ "$backupSchemaSucceeded" != 0 && "$backupDataSucceeded" != 0 ]]
then
  echo >&2 "$COMMAND: Project database '$REMOTE_JENKINS_PROJECT_DATABASE' backup failed."
  exit 1
fi

echo >&2 "$COMMAND: Project database '$REMOTE_JENKINS_PROJECT_DATABASE' backup successful."

# backup, build and deploy project
echo "Backup, build and deploy ${JENKINS_JOB_NAME}..."

backupAndBuildAndDeployRemoteProject

exit 0

## -- finish
